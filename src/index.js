"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const program = require("commander");
const inquirer_1 = require("inquirer");
const contact_1 = require("./contact");
const questions = [
    {
        type: "input",
        name: "firstname",
        message: "Enter firstname ..",
    },
    {
        type: "input",
        name: "lastname",
        message: "Enter lastname ..",
    },
    {
        type: "input",
        name: "phone",
        message: "Enter phone number ..",
    },
    {
        type: "input",
        name: "email",
        message: "Enter email address ..",
    },
];
program.version("0.0.1").description("contact management system");
program
    .command("addContact")
    .alias("a")
    .description("Add a contact")
    .action(() => {
    inquirer_1.prompt(questions).then((answers) => contact_1.addContact(answers));
});
program
    .command("getContact <name>")
    .alias("r")
    .description("Get contact")
    .action((name) => contact_1.getContact(name));
program
    .command("updateContact <_id>")
    .alias("U")
    .description("Update contact")
    .action((_id) => {
    inquirer_1.prompt(questions).then((answers) => contact_1.updateContact(_id, answers));
});
program
    .command("deleteContact <_id>")
    .alias("D")
    .description("Delete contact")
    .action((_id) => contact_1.deleteContact(_id));
program
    .command("getContactList")
    .alias("L")
    .description("List contacts")
    .action(() => contact_1.getContactList());
// Assert that a VALID command is provided 
if (!process.argv.slice(2).length) {
    program.outputHelp();
    process.exit();
}
program.parse(process.argv);
