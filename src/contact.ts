import * as mongoose from "mongoose"; // An Object-Document Mapper for Node.js
import * as assert from "assert"; // N.B: Assert module comes bundled with NodeJS.

// mongoose.Promise = global.Promise; // Allows us to use Native promises without throwing error.

// Connect to a single MongoDB instance. The connection string could be that of remote server
// We assign the connection instance to a constant to be used later in closing the connection
const db = mongoose.connect("mongodb://localhost:27017/contact-manager");

// Convert value to to lowercase
function toLower(v) {
  return v.toLowerCase();
}

// Define a contact Schema
const contactSchema = new mongoose.Schema({
  firstname: { type: String, set: toLower },
  lastname: { type: String, set: toLower },
  phone: { type: String, set: toLower },
  email: { type: String, set: toLower },
});

// Define model as an interface with the database
const Contact = mongoose.model("Contact", contactSchema);

/**
 * @function  [addContact]
 * @returns {String} Status
 */
export const addContact = async (contact) => {
  const res = await Contact.create(contact);
  // db.close();
  return res;
};

/**
 * @function  [getContact]
 * @returns {Json} contacts
 */
export const getContact = async (name) => {
  // Define search criteria
  const search = new RegExp(name, "i");

  const contacts = await Contact.find({
    $or: [{ firstname: search }, { lastname: search }],
  });

  console.log(contacts);

  // db.disconnect();

  return contacts;
};

/**
 * @function  [getContactList]
 * @returns {Sting} status
 */
export const updateContact = async (_id, contact) => {
  const res = await Contact.update({ _id }, contact);

  return res;
};

/**
 * @function  [deleteContact]
 * @returns {String} status
 */
export const deleteContact = async (_id) => {
  const res = await Contact.remove({ _id });

  return res;
};

/**
 * @function  [getContactList]
 * @returns [contactlist] contacts
 */
export const getContactList = async () => {
  console.info("here");
  const result = await Contact.find();
  console.log(result);
  return result;
};
