import * as program from "commander";
import { prompt } from "inquirer";

import {
  addContact,
  getContact,
  getContactList,
  updateContact,
  deleteContact,
} from "./contact";

const questions = [
  {
    type: "input",
    name: "firstname",
    message: "Enter firstname ..",
  },
  {
    type: "input",
    name: "lastname",
    message: "Enter lastname ..",
  },
  {
    type: "input",
    name: "phone",
    message: "Enter phone number ..",
  },
  {
    type: "input",
    name: "email",
    message: "Enter email address ..",
  },
];

program.version("0.0.1").description("contact management system");

program
  .command("addContact")
  .alias("a")
  .description("Add a contact")
  .action(() => {
    prompt(questions).then((answers) => addContact(answers));
  });

program
  .command("getContact <name>")
  .alias("r")
  .description("Get contact")
  .action((name) => getContact(name));

program
  .command("updateContact <_id>")
  .alias("U")
  .description("Update contact")
  .action((_id) => {
    prompt(questions).then((answers) => updateContact(_id, answers));
  });

program
  .command("deleteContact <_id>")
  .alias("D")
  .description("Delete contact")
  .action((_id) => deleteContact(_id));

program
  .command("getContactList")
  .alias("L")
  .description("List contacts")
  .action(() => getContactList());

// Assert that a VALID command is provided 
if (!process.argv.slice(2).length ) {
  program.outputHelp();
  process.exit();
}
program.parse(process.argv)
