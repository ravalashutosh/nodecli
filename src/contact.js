"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getContactList = exports.deleteContact = exports.updateContact = exports.getContact = exports.addContact = void 0;
const mongoose = require("mongoose"); // An Object-Document Mapper for Node.js
// mongoose.Promise = global.Promise; // Allows us to use Native promises without throwing error.
// Connect to a single MongoDB instance. The connection string could be that of remote server
// We assign the connection instance to a constant to be used later in closing the connection
const db = mongoose.connect("mongodb://localhost:27017/contact-manager");
// Convert value to to lowercase
function toLower(v) {
    return v.toLowerCase();
}
// Define a contact Schema
const contactSchema = new mongoose.Schema({
    firstname: { type: String, set: toLower },
    lastname: { type: String, set: toLower },
    phone: { type: String, set: toLower },
    email: { type: String, set: toLower },
});
// Define model as an interface with the database
const Contact = mongoose.model("Contact", contactSchema);
/**
 * @function  [addContact]
 * @returns {String} Status
 */
exports.addContact = (contact) => __awaiter(void 0, void 0, void 0, function* () {
    const res = yield Contact.create(contact);
    // db.close();
    return res;
});
/**
 * @function  [getContact]
 * @returns {Json} contacts
 */
exports.getContact = (name) => __awaiter(void 0, void 0, void 0, function* () {
    // Define search criteria
    const search = new RegExp(name, "i");
    const contacts = yield Contact.find({
        $or: [{ firstname: search }, { lastname: search }],
    });
    console.log(contacts);
    // db.disconnect();
    return contacts;
});
/**
 * @function  [getContactList]
 * @returns {Sting} status
 */
exports.updateContact = (_id, contact) => __awaiter(void 0, void 0, void 0, function* () {
    const res = yield Contact.update({ _id }, contact);
    return res;
});
/**
 * @function  [deleteContact]
 * @returns {String} status
 */
exports.deleteContact = (_id) => __awaiter(void 0, void 0, void 0, function* () {
    const res = yield Contact.remove({ _id });
    return res;
});
/**
 * @function  [getContactList]
 * @returns [contactlist] contacts
 */
exports.getContactList = () => __awaiter(void 0, void 0, void 0, function* () {
    console.info("here");
    const result = yield Contact.find();
    console.log(result);
    return result;
});
